#include "task.h"
#include <iostream>

Task::Task() :
    iterations(0),
    completed(false)
{}

void Task::setDuration(const std::string &department, int d) {
    durations[department] = d;
}

int Task::getDuration(const std::string &department) const {
    return durations.at(department);
}

void Task::incIterations() {
    iterations++;
    std::cout << "Задача уже делается " << iterations << " дней" << std::endl;
}

void Task::resetIterations() {
    iterations = 0;
    std::cout << "Работа над задачей начата" << std::endl;
}

int Task::getIterations() const {
    return iterations;
}

bool Task::isFinished(const std::string &department) const {
    return iterations >= durations.at(department);
}

void Task::setCompleted() {
    completed = true;
}

bool Task::isCompleted() const {
    return completed;
}
