#ifndef STEPPER_H
#define STEPPER_H

#include "subscriber.h"
#include <vector>

class Employee;
class Department;
class Task;

class Stepper : public Subscriber {
public:
    Stepper();
    ~Stepper();

    bool step();

    void printRating();

    void update(Task *task) override;

private:
    int current_step;
    std::vector<Employee *> employees;
    std::vector<Department *> departments;
    std::vector<Task *> tasks;
};

#endif // STEPPER_H
