TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    task.cpp \
    employee.cpp \
    department.cpp \
    stepper.cpp

HEADERS += \
    task.h \
    employee.h \
    department.h \
    subscriber.h \
    stepper.h
