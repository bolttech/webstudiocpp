#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include "task.h"

#include <string>

class Department;

class Employee {
public:
    Employee(const std::string &name, Department *department);

    std::string getName() const;

    void addTask(Task *task);
    bool hasTask() const;

    void setBoss(Employee *boss);

    void incPoints();
    void decPoints();
    int getPoints() const;

    void step();

private:
    void onTaskFinished();

    std::string name;
    int success_rate;
    int points;
    Task *current_task;
    Department *department;
    Employee *boss;
};

#endif // EMPLOYEE_H
