#ifndef TASK_H
#define TASK_H

#include <string>
#include <map>

class Task {
public:
    Task();

    void setDuration(const std::string &department, int iterations);
    int getDuration(const std::string &department) const;
    void incIterations();
    void resetIterations();
    int getIterations() const;
    bool isFinished(const std::string &department) const;

    void setCompleted();
    bool isCompleted() const;

private:
    int iterations;
    std::map<std::string, int> durations;
    bool completed;
};

#endif // TASK_H
