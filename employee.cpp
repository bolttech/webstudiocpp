#include "employee.h"
#include "department.h"
#include <stdlib.h>
#include <iostream>
#include <stdexcept>

Employee::Employee(const std::string &name, Department *department) :
    name(name),
    success_rate(rand() % (95 - 75 + 1) + 75),
    points(0),
    current_task(nullptr),
    department(department),
    boss(nullptr)
{}

std::string Employee::getName() const {
    return name;
}

void Employee::addTask(Task *task) {
    if (current_task) {
        throw std::logic_error("This employee is already busy");
    }

    current_task = task;
}

bool Employee::hasTask() const {
    return current_task;
}

void Employee::setBoss(Employee *b) {
    boss = b;
}

void Employee::incPoints() {
    points++;
    if (boss) {
        boss->incPoints();
    }
}

void Employee::decPoints() {
    points--;
    if (boss) {
        boss->decPoints();
    }
}

int Employee::getPoints() const {
    return points;
}

void Employee::step() {
    if (current_task) {
        std::cout << "Сотрудник " << getName() << " работает на задачей" << std::endl;
        current_task->incIterations();
        if (current_task->isFinished(department->getName())) {
            int chance = rand() % 100;
            if (chance <= success_rate) {
                onTaskFinished();
            }
        }
    }
}

void Employee::onTaskFinished() {
    Task *task = current_task;
    if (current_task->getIterations() > current_task->getDuration(department->getName())) {
        std::cout << "Сотрудник " << getName() << " выполнил задачу с опозданием" << std::endl;
        decPoints();
    } else {
        std::cout << "Сотрудник " << getName() << " выполнил задачу в срок" << std::endl;
        incPoints();
    }
    current_task = nullptr;
    department->notify(task);
}
