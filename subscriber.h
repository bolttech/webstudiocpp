#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

class Task;

class Subscriber {
public:
    virtual ~Subscriber() {}

    virtual void update(Task *) = 0;
};

#endif // SUBSCRIBER_H
