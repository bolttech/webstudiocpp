#include "department.h"
#include "employee.h"
#include <iostream>

Department::Department(const std::string &name) :
    name(name),
    boss(nullptr)
{}

std::string Department::getName() const {
    return name;
}

void Department::addEmployee(Employee *employee) {
    employees.push_back(employee);

    if (boss) {
        employee->setBoss(boss);
    }
}

void Department::setBoss(Employee *b) {
    for (auto &employee : employees) {
        if (employee != b) {
            employee->setBoss(b);
        }
    }

    boss = b;
}

void Department::addTask(Task *task) {
    Employee *free_employee = getFreeEmployee();

    if (!free_employee) {
        unassigned_tasks.push(task);
    } else {
        free_employee->addTask(task);
    }
}

Employee *Department::getFreeEmployee() const {
    Employee *free_employee = nullptr;
    for (auto &employee : employees) {
        if (!employee->hasTask()) {
            free_employee = employee;
            break;
        }
    }
    return free_employee;
}

void Department::subscribe(Subscriber *subscriber) {
    subscribers.push_back(subscriber);
}

void Department::notify(Task *task) {
    task->resetIterations();

    for (auto &subscriber : subscribers) {
        subscriber->update(task);
    }
}

void Department::update(Task *task) {
    addTask(task);
}

void Department::step() {
    std::cout << "Обработка задач в отделе " << getName() << std::endl;
    while (getFreeEmployee() && unassigned_tasks.size() > 0) {
        std::cout << "Назначили отложенную задачу сотруднику" << std::endl;
        addTask(unassigned_tasks.front());
        unassigned_tasks.pop();
    }

    for (auto &employee : employees) {
        employee->step();
    }
}
