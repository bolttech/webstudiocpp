#include "stepper.h"
#include "department.h"
#include "employee.h"
#include "task.h"
#include <stdlib.h>
#include <iostream>

Stepper::Stepper() :
    current_step(0)
{
    Department *design = new Department("Design");
    departments.push_back(design);

    Department *frontend = new Department("Frontend");
    departments.push_back(frontend);

    Department *backend = new Department("Backend");
    departments.push_back(backend);

    Employee *emp = new Employee("Big Boss", design);
    design->addEmployee(emp);
    design->setBoss(emp);
    employees.push_back(emp);

    emp = new Employee("Asd Asd", design);
    design->addEmployee(emp);
    employees.push_back(emp);

    emp = new Employee("Qwe Qwe", design);
    design->addEmployee(emp);
    employees.push_back(emp);

    emp = new Employee("Zxc Zxc", design);
    design->addEmployee(emp);
    employees.push_back(emp);

    emp = new Employee("Fgh Fgh", design);
    design->addEmployee(emp);
    employees.push_back(emp);

    emp = new Employee("Rty Rty", frontend);
    frontend->addEmployee(emp);
    employees.push_back(emp);

    emp = new Employee("Vbn Vbn", frontend);
    frontend->addEmployee(emp);
    employees.push_back(emp);

    emp = new Employee("Boss Big", frontend);
    frontend->addEmployee(emp);
    frontend->setBoss(emp);
    employees.push_back(emp);

    emp = new Employee("Boss Boss", backend);
    backend->addEmployee(emp);
    backend->setBoss(emp);
    employees.push_back(emp);

    emp = new Employee("Ghj Ghj", backend);
    backend->addEmployee(emp);
    employees.push_back(emp);

    design->subscribe(frontend);
    frontend->subscribe(backend);
    backend->subscribe(this);
}

Stepper::~Stepper() {
    for (auto &department : departments) {
        delete department;
    }

    for (auto &employee : employees) {
        delete employee;
    }

    for (auto &task : tasks) {
        delete task;
    }
}

void Stepper::printRating() {
    bool done = false;

    while (!done) {
        done = true;

        for (int i = 0; i < employees.size() - 1; i++) {
            if (employees[i]->getPoints() < employees[i+1]->getPoints()) {
                Employee *tmp = employees[i];
                employees[i] = employees[i+1];
                employees[i+1] = tmp;
                done = false;
            }
        }
    }

    for (auto &employee : employees) {
        std::cout << employee->getName() << ": " << employee->getPoints() << std::endl;
    }
}

void Stepper::update(Task *task) {
    task->setCompleted();
}

bool Stepper::step() {
    std::cout << "Начался новый день" << std::endl;
    if (current_step++ < 10) {
        std::cout << "Пришел новый заказ" << std::endl;
        Task *task = new Task();
        task->setDuration("Design", rand() % (7 - 1 + 1) + 1);
        task->setDuration("Frontend", rand() % (7 - 1 + 1) + 1);
        task->setDuration("Backend", rand() % (7 - 1 + 1) + 1);

        departments[0]->addTask(task);

        tasks.push_back(task);
    }

    for (auto &department : departments) {
        department->step();
    }

    bool has_tasks = false;
    for (auto &task : tasks) {
        if (!task->isCompleted()) {
            has_tasks = true;
            break;
        }
    }

    return has_tasks || current_step < 10;
}
