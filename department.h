#ifndef DEPARTMENT_H
#define DEPARTMENT_H

#include "subscriber.h"
#include <string>
#include <vector>
#include <queue>

class Task;
class Employee;

class Department : public Subscriber {
public:
    Department(const std::string &name);

    std::string getName() const;

    void addEmployee(Employee *employee);
    void setBoss(Employee *boss);
    void addTask(Task *task);

    void update(Task *task) override;
    void subscribe(Subscriber *subscriber);
    void notify(Task *task);

    void step();

private:
    Employee *getFreeEmployee() const;

    std::string name;
    std::vector<Employee *> employees;
    Employee *boss;
    std::queue<Task *> unassigned_tasks;

    std::vector<Subscriber *> subscribers;
};

#endif // DEPARTMENT_H
