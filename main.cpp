#include "stepper.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(nullptr));

    Stepper stepper;

    while (stepper.step()) ;

    stepper.printRating();

    return 0;
}
